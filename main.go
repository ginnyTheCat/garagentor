package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/stianeikeland/go-rpio/v4"
)

var token = os.Getenv("TOKEN")

var pin = rpio.Pin(21)

func handle(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	if r.URL.Query().Get("token") != token {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	pin.Low()
	time.Sleep(2 * time.Second)
	pin.High()

	fmt.Println("Toggle")

	w.WriteHeader(http.StatusOK)
}

func main() {
	err := rpio.Open()
	if err != nil {
		log.Fatal(err)
	}
	defer rpio.Close()
	pin.Output()

	fmt.Println("Listening")

	http.HandleFunc("/", handle)
	err = http.ListenAndServe(":80", nil)
	if err != nil {
		log.Fatal(err)
	}
}
